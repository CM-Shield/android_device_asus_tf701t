# Inherit device configuration for tf701t.
$(call inherit-product, device/asus/tf701t/full_tf701t.mk)

# Inherit some common cyanogenmod stuff.
$(call inherit-product, vendor/cm/config/common_full_tablet_wifionly.mk)

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=K00C \
    BUILD_FINGERPRINT=asus/US_epad/K00C:4.4.2/KOT49H/US_epad-11.4.1.29-20141218:user/release-keys \
    PRIVATE_BUILD_DESC="US_epad-user 4.4.2 KOT49H US_epad-11.4.1.29-20141218 release-keys"

PRODUCT_NAME := cm_tf701t
PRODUCT_DEVICE := tf701t
